package fr.nuroz.rushdragon.rushdragon;

import fr.nuroz.rushdragon.rushdragon.commands.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wither;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public final class RushDragon extends JavaPlugin implements Listener {
    public static boolean isRunning = false;
    public static LocalDateTime startTime = null;
    public static LocalDateTime endTime = null;
    public static List<String> mobKillList;

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);

        getCommand("start").setExecutor(new Start());
        getCommand("time").setExecutor(new Time());
        getCommand("here").setExecutor(new Here());
        getCommand("add").setExecutor(new Add());
        getCommand("remove").setExecutor(new Remove());
        getCommand("checklist").setExecutor(new Checklist());
        getCommand("stop").setExecutor(new Stop());
        getCommand("inv").setExecutor(new Inv());

        System.out.println("plugin on");
    }

    @Override
    public void onDisable() {
        System.out.println("plugin off");
    }

    public static ArrayList<Integer> getDifferenceTime() {
        ArrayList<Integer> result = new ArrayList<>();
        Duration duree = Duration.between(startTime, LocalDateTime.now());
        result.add(0,(int) Math.floor(duree.getSeconds() / 3600));
        result.add(1,(int) (Math.floor(duree.getSeconds() / 60) % 60));
        result.add(2,(int) duree.getSeconds() % 60);

        return result;
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player p = event.getPlayer();
        p.setOp(false);
        p.setGameMode(GameMode.SURVIVAL);
        event.setJoinMessage(p.getName() + " vien casser du dragon");
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player p = event.getPlayer();
        event.setQuitMessage(p.getName() + " a eu peur de la fureur du dragon");
    }

    public static void endGame() {
        for(Player p : Bukkit.getOnlinePlayers())
        {
            ArrayList<Integer> difference = getDifferenceTime();
            p.sendTitle("Vous avez gagner!",  difference.get(0) + ":" + difference.get(1) + ":" + difference.get(2));
            p.setGameMode(GameMode.CREATIVE);
            endTime = LocalDateTime.now();
            isRunning = false;
        }
    }

    @EventHandler
    public void onEntityDeath(EntityDeathEvent event) {
        if(event.getEntity() instanceof EnderDragon) {
            if (isRunning) {
                if (mobKillList.contains("dragon")) {
                    mobKillList.remove("dragon");
                }

                if (mobKillList.size() == 0) {
                    endGame();
                }
            }
        } else if(event.getEntity() instanceof Wither) {
            if(isRunning) {
                if(mobKillList.contains("wither")) {
                    mobKillList.remove("wither");
                }

                if(mobKillList.size() == 0) {
                    endGame();
                }
            }
        } else if(event.getEntity() instanceof Player) {
            List<Player> playerList = new ArrayList<>(Bukkit.getOnlinePlayers());
            for(Player p : playerList) {
                p.playSound(p.getLocation(), Sound.ENTITY_WITHER_SPAWN,1f, 1f);
            }
        }
    }
}
