package fr.nuroz.rushdragon.rushdragon.commands;

import fr.nuroz.rushdragon.rushdragon.RushDragon;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Stop implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;

            if(p.getName().equals("NuRoZ")) {
                if(RushDragon.isRunning) {
                    ArrayList<Integer> difference = RushDragon.getDifferenceTime();

                    Bukkit.broadcastMessage("Fin du rush par arret forcé temps passé: " + difference.get(0) + ":" + difference.get(1) + ":" + difference.get(2));
                    RushDragon.isRunning = false;
                    RushDragon.endTime = LocalDateTime.now();
                } else {
                    p.sendMessage("aucune partie en cours...");
                }
            } else {
                p.sendMessage("vous n'avez pas les autorisations");
            }
        }
        return false;
    }
}
