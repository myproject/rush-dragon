package fr.nuroz.rushdragon.rushdragon.commands;

import fr.nuroz.rushdragon.rushdragon.RushDragon;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.time.Duration;
import java.util.ArrayList;

public class Time implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;

            if(RushDragon.startTime != null) {
                if(RushDragon.isRunning) {
                    ArrayList<Integer> difference = RushDragon.getDifferenceTime();
                    p.sendMessage("temps: " + difference.get(0) + ":" + difference.get(1) + ":" + difference.get(2));
                }
                else {
                    Duration duree = Duration.between(RushDragon.startTime, RushDragon.endTime);
                    int hour = (int) Math.floor(duree.getSeconds() / 3600);
                    int min = (int) (Math.floor(duree.getSeconds() / 60) % 60);
                    int sec = (int) duree.getSeconds() % 60;

                    p.sendMessage("temps: " + hour + ":" + min + ":" + sec);
                }
            }
        }
        return false;
    }
}
