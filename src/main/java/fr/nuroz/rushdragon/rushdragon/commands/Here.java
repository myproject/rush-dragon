package fr.nuroz.rushdragon.rushdragon.commands;

import fr.nuroz.rushdragon.rushdragon.RushDragon;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Here implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;

            if(RushDragon.isRunning) {
                Location pLocation = p.getLocation();
                Bukkit.broadcastMessage(p.getName() + " est en X: " + Math.floor(pLocation.getX()) + ", Y: " + Math.floor(pLocation.getY()) + ", Z:" + Math.floor(pLocation.getZ()) + ", world: " + pLocation.getWorld().getEnvironment());
            } else {
                p.sendMessage("Aucun rush n'est démarré");
            }
        }

        return false;
    }
}
