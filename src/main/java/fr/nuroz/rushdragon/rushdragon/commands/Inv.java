package fr.nuroz.rushdragon.rushdragon.commands;

import fr.nuroz.rushdragon.rushdragon.RushDragon;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Inv implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;

            if (RushDragon.isRunning) {
                int enderPearl = 0;
                int blazeRod = 0;
                int eyeOfEnder = 0;

                for(Player player : Bukkit.getOnlinePlayers()) {
                    ItemStack[] items = player.getInventory().getContents();
                    for (ItemStack item : items)
                    {
                        if(item != null)
                        {
                            if(item.getType() == Material.ENDER_PEARL) {
                                enderPearl += item.getAmount();
                            } else if(item.getType() == Material.BLAZE_ROD) {
                                blazeRod += item.getAmount();
                            } else if(item.getType() == Material.ENDER_EYE) {
                                eyeOfEnder += item.getAmount();
                            }
                        }
                    }
                }

                p.sendMessage("liste des objets:");
                p.sendMessage("Ender pearl: " + enderPearl);
                p.sendMessage("Blaze rod: " + blazeRod);
                p.sendMessage("Eye of Ender: " + eyeOfEnder);
            } else {
                p.sendMessage("Aucun rush n'est démarré");
            }
        }
        return false;
    }
}
