package fr.nuroz.rushdragon.rushdragon.commands;

import fr.nuroz.rushdragon.rushdragon.RushDragon;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Start implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;

            if(p.getName().equals("NuRoZ")) {
                p.setOp(true);
                p.performCommand("scoreboard objectives add Morts deathCount");
                p.performCommand("scoreboard objectives add Healths health");
                p.performCommand("scoreboard objectives setdisplay sidebar Morts");
                p.performCommand("scoreboard objectives setdisplay list Healths");
                p.performCommand("scoreboard players set @a Morts 0");
                p.getWorld().setTime(0);
                p.setOp(false);

                LocalDateTime startTime = LocalDateTime.now();
                Bukkit.broadcastMessage("le rush a commencé: il est " + startTime.format(DateTimeFormatter.ofPattern("HH:mm:ss")));
                RushDragon.startTime = startTime;
                RushDragon.isRunning = true;
                RushDragon.endTime = null;
                RushDragon.mobKillList = new ArrayList<>();
                RushDragon.mobKillList.add("dragon");
            } else {
                p.sendMessage("vous n'avez pas les autorisations");
            }
        }
        return false;
    }
}
