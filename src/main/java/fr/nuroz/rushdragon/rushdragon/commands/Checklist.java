package fr.nuroz.rushdragon.rushdragon.commands;

import fr.nuroz.rushdragon.rushdragon.RushDragon;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Checklist implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            p.sendMessage("mob restant:");
            String res = "";
            for(String mobs: RushDragon.mobKillList) {
                res += mobs + ", ";
            }
            p.sendMessage(res.substring(0, res.length() - 2));
        }

        return false;
    }
}
