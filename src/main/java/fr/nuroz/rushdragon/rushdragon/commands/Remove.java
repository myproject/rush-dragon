package fr.nuroz.rushdragon.rushdragon.commands;

import fr.nuroz.rushdragon.rushdragon.RushDragon;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Remove implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;

            if(p.getName().equals("NuRoZ")) {
                if (RushDragon.isRunning) {
                    if (args[0].equals("dragon") || args[0].equals("wither")) {
                        RushDragon.mobKillList.remove(args[0]);
                        if (RushDragon.mobKillList.size() == 0) {
                            RushDragon.endGame();
                        }
                    } else {
                        p.sendMessage("Veuillez saisir wither ou dragon");
                    }
                } else {
                    p.sendMessage("aucune partie en cours...");
                }
            } else {
                p.sendMessage("vous n'avez pas les autorisations");
            }
        }

        return false;
    }
}
