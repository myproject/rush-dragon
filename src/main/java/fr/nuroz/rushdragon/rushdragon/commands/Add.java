package fr.nuroz.rushdragon.rushdragon.commands;

import fr.nuroz.rushdragon.rushdragon.RushDragon;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Add implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;

            if(p.getName().equals("NuRoZ")) {
                if(RushDragon.isRunning) {
                    if(args[0].equals("wither") || args[0].equals("dragon")) {
                        RushDragon.mobKillList.add(args[0]);
                        p.sendMessage("mob ajouté: " + args[0]);
                    } else {
                        p.sendMessage("Veuillez saisir wither ou dragon");
                    }
                } else {
                    p.sendMessage("Aucun rush n'est démarré");
                }
            } else {
                p.sendMessage("vous n'avez pas les autorisations");
            }
        }

        return false;
    }
}
